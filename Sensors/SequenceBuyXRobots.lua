local sensorInfo = {
	name = "SequenceBuyXRobots",
	desc = "Poda",
	author = "PepeAmpere",
	date = "2018-04-16",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
--local ID = 0
function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end
-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load


return function(unitName,unitCount)

	local koupeno = 0
	local startMetal = Sensors.core.TeamMetal().currentLevel
	--Spring.Echo("metal:" .. global.info.buy[unitName])
	if unitCount > 0 then
		while koupeno < unitCount and global.info.buy[unitName] < startMetal do
			message.SendRules({
			subject = "swampdota_buyUnit",
					data = {
							unitName = unitName
							},
			})
			koupeno = koupeno + 1
			startMetal = startMetal - global.info.buy[unitName]
			
		
		end
		
	
	
	end 
	
    
	
    return koupeno
end

