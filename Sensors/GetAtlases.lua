local sensorInfo = {
	name = "GetAtlases",
	desc = "Sends data to example debug widget",
	author = "PepeAmpere",
	date = "2018-04-16",
	license = "MIT",
}


local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
--local ID = 0
function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end
return function()
	
	local atlases = Sensors.core.FilterUnitsByCategory(Spring.GetTeamUnits(Spring.GetMyTeamID() ),Categories.Common.transports)
	local atlasesRev = {}
	for index,unitID in pairs(atlases) do
		atlasesRev[unitID] = "free"	
	end
	
	return atlasesRev
end