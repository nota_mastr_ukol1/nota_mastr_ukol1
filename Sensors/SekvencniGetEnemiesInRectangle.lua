local sensorInfo = {
	name = "SekvencniGetEnemiesInRectangle",
	desc = "Poda",
	author = "PepeAmpere",
	date = "2018-04-16",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
--local ID = 0
function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function(area)


	local myID = Spring.GetMyTeamID()
	local teams = Spring.GetTeamList()
	local enemyTeams = {}
	local allEnemies = {}
	local xmin = area[1]
	local zmin = area[2]
	local xmax = area[3]
	local zmax = area[4]
	
	
	for i=1,#teams do
		if not Spring.AreTeamsAllied(myID,teams[i]) then
			enemyTeams[#enemyTeams+1] = teams[i]
		end	
	end
	bb.et = enemyTeams
	for	i=1,#enemyTeams do
		local enemyUnits = Spring.GetUnitsInRectangle(xmin,zmin,xmax,zmax,enemyTeams[i])
		
		for j=1,#enemyUnits do
			allEnemies[enemyUnits[j]] = {}
			allEnemies[enemyUnits[j]].position = Vec3(Spring.GetUnitPosition(enemyUnits[j]))
			--allEnemies[enemyUnits[j]].name = UnitDefs[Spring.GetUnitDefID(enemyUnits[j])].name
			
			--allEnemies[enemyUnits[j]].canmoe = UnitDefs[Spring.GetUnitDefID(enemyUnits[j])].canMove
		end	
	end
	
    return allEnemies
end

