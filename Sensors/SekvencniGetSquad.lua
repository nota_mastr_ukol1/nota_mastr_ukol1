local sensorInfo = {
	name = "SekvencniGetSquad",
	desc = "Poda",
	author = "PepeAmpere",
	date = "2018-04-16",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
--local ID = 0
function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function(kompozice,cisloSquadu)
	--kompozice = {jmeno = pocet, jmeno2 = pocet2,...}
	
	local squad = {}
	local ready = true
	local chybi = {}
	local byName = {}
	--[[
	local mame = {}
	local uzVeSquadu = Sensors.nota_mastr_ukol1.GetMatchingUnits({squadID = cisloSquadu})
	if Sensors.nota_mastr_ukol1.GetCount(uzVeSquadu) > 0 then
		--mame nejake existujici prirazene jednotky
		for unitID,vlastnosti in pairs(uzVeSquadu) do
			if not mame.vlastnosti.name then
				mame.vlastnosti.name = 1
			else
				mame.vlastnosti.name = mame.vlastnosti.name + 1
			end
		
		end
	end 
	]]--
	for jmeno,pocet in pairs(kompozice) do
		local ziskano = Sensors.nota_mastr_ukol1.GetMatchingUnits({name = jmeno}) ---get(jmeno)
		local uzVeSquadu = Sensors.nota_mastr_ukol1.GetMatchingUnits({name = jmeno, squadID = cisloSquadu})
		local added = 0 + Sensors.nota_mastr_ukol1.GetCount(uzVeSquadu)
		
		byName[jmeno] = {}
		
		for unitID,vlastnosti in pairs(uzVeSquadu) do
			byName[jmeno][Sensors.nota_mastr_ukol1.GetCount(byName[jmeno])+1] = unitID
			squad[unitID] = vlastnosti
		end
		
		
		for unitID,vlastnosti in pairs(ziskano) do
			if added == pocet then break end 
			if vlastnosti.designation == "free" or vlastnosti.designation == "new" or not vlastnosti.squadID == cisloSquadu then 
				added = added + 1 
				vlastnosti.designation = "deepstriker"
				vlastnosti.squadID = cisloSquadu
				byName[jmeno][added] = unitID
				squad[unitID] = vlastnosti
			end
			
		end
		
		if added < pocet then 
			chybi[jmeno] = pocet - added
			ready = false
		end
	
	end
	
	if not ready then 
		squad.chybi = chybi
	end
	squad.ready = ready
    squad.byName = byName
	
    return squad
end

