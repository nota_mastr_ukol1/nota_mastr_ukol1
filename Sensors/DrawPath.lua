local sensorInfo = {
	name = "DrawSafePath",
	desc = "Sends data to example debug widget",
	author = "PepeAmpere",
	date = "2018-04-16",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
--local ID = 0
function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end
function ipm(x,z,xMax)
	return (z-1)*xMax + x
end
function g2p(path)
	local rp = {}
	for i=1,#path do
		rp[i] = Vec3(path[i].x*50,Spring.GetGroundOrigHeight(path[i].x*50,path[i].z*50),path[i].z*50)
	end
	return rp

end
function positionToGridPoint(pos,gridStep)
	local baseX = pos.x
	local baseZ = pos.z
	
	local retX = (( baseX - ( baseX % gridStep ) ) / gridStep ) + 1
	local retZ = (( baseZ - ( baseZ % gridStep ) ) / gridStep ) + 1
	
	
	return Vec3(retX,0,retZ)  --{retX,retZ}

end
-- @description return current wind statistics
return function(mapaCest,finish,maxX,gridStep)
	local cil = positionToGridPoint(finish, gridStep)
	local finishX = cil.x
	local finishZ = cil.z
	local predchozi = mapaCest[ipm(finishX,finishZ,maxX)].cesta
	local totdis = mapaCest[ipm(finishX,finishZ,maxX)].delka
	local returnPath = {}
	bb.drawdebug = mapaCest[ipm(finishX,finishZ,maxX)]
	
	

	
	
	bb.pth = {}
	bb.pth[totdis+1] = Vec3(mapaCest[ipm(finishX,finishZ,maxX)].x,mapaCest[ipm(finishX,finishZ,maxX)].danger,mapaCest[ipm(finishX,finishZ,maxX)].z)
	returnPath[totdis+1] = Vec3(mapaCest[ipm(finishX,finishZ,maxX)].x,mapaCest[ipm(finishX,finishZ,maxX)].danger,mapaCest[ipm(finishX,finishZ,maxX)].z)
	for c=1,totdis do
		bb.pth[totdis-c+1] = Vec3(mapaCest[predchozi].x,mapaCest[predchozi].danger,mapaCest[predchozi].z)
		returnPath[totdis-c+1] = Vec3(mapaCest[predchozi].x,mapaCest[predchozi].danger,mapaCest[predchozi].z)
		predchozi = mapaCest[predchozi].cesta
		
	end
	
	
	local cesta = returnPath--
	bb.debugpath = g2p(returnPath)--cesta
	if #cesta > 0 then
		local ID = 0
		
		for i=1,#cesta-1 do
			ID = ID+1
			local x = cesta[i].x*50
		
			
			local z = cesta[i].z*50
			
			local y = Spring.GetGroundOrigHeight(x,z)
			local x2 = cesta[i+1].x*50
			local z2 = cesta[i+1].z*50
			local y2 = Spring.GetGroundOrigHeight(x2,z2)
			
			if (Script.LuaUI('exampleDebug_update')) then
				Script.LuaUI.exampleDebug_update(
					ID, -- key
					{	-- data
						startPos = Vec3(x,y,z), 
						endPos = Vec3(x2,y2,z2)
					}
				)
			end
		end
		return {	-- data
					startPos = Vec3(cesta[1].x*50,Spring.GetGroundOrigHeight(cesta[1].x*50,cesta[1].z*50),cesta[1].z*50), 
					endPos = Vec3(cesta[#cesta].x*50,Spring.GetGroundOrigHeight(cesta[#cesta].x*50,cesta[#cesta].z*50),cesta[#cesta].z*50)
				}
	end
end 