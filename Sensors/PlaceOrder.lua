local sensorInfo = {
	name = "PlaceOrder",
	desc = "Ulozi objednavku",
	author = "PepeAmpere",
	date = "2018-04-16",
	license = "MIT",
}


local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
--local ID = 0
function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end
return function(objednavka)
	
	local order = {}	
	if #objednavka == 1 then
		order = objednavka[1]
		order.typ = "basic"
	else
		order.typ = "combined"
		for i,nakup in ipairs(objednavka) do		
			order[i] = nakup
			order[i].typ = "basic"
		end
	end
	if not global.nakupni_seznam then global.nakupni_seznam = {} end 
	if not global.UID_objednavek then global.UID_objednavek = 0 end 
	global.UID_objednavek = global.UID_objednavek + 1
	order.UID = global.UID_objednavek
	global.nakupni_seznam[#global.nakupni_seznam + 1] = order
	return order
end