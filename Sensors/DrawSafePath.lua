local sensorInfo = {
	name = "DrawSafePath",
	desc = "Sends data to example debug widget",
	author = "PepeAmpere",
	date = "2018-04-16",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
--local ID = 0
function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return current wind statistics
return function(cesta)
	if #cesta > 0 then
		local ID = 0
		
		for i=1,#cesta-1 do
			ID = ID+1
			local x = cesta[i].x*50
		
			
			local z = cesta[i].z*50
			
			local y = Spring.GetGroundOrigHeight(x,z)
			local x2 = cesta[i+1].x*50
			local z2 = cesta[i+1].z*50
			local y2 = Spring.GetGroundOrigHeight(x2,z2)
			
			if (Script.LuaUI('exampleDebug_update')) then
				Script.LuaUI.exampleDebug_update(
					ID, -- key
					{	-- data
						startPos = Vec3(x,y,z), 
						endPos = Vec3(x2,y2,z2)
					}
				)
			end
		end
		return {	-- data
					startPos = Vec3(cesta[1].x*50,Spring.GetGroundOrigHeight(cesta[1].x*50,cesta[1].z*50),cesta[1].z*50), 
					endPos = Vec3(cesta[#cesta].x*50,Spring.GetGroundOrigHeight(cesta[#cesta].x*50,cesta[#cesta].z*50),cesta[#cesta].z*50)
				}
	end
end