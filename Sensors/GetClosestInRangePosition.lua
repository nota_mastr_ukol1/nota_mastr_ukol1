
local sensorInfo = {
	name = "GetClosestInRangePosition",
	desc = "Poda",
	author = "PepeAmpere",
	date = "2018-04-16",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
--local ID = 0
function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function(AttackerID,TargetID)
	
	local aid = AttackerID
	local tid = TargetID

	if not aid then 
		Spring.Echo("attacker is nil")
		return Vec3(0,0,0)
	end
	

	if not Spring.ValidUnitID(aid) then	
		Spring.Echo("Invalid attackerID --> "..aid)
	end
	
	
	local aX, aY, aZ = Spring.GetUnitPosition(aid)	
	local attackerPos = Vec3(aX,aY,aZ)
	local pos = attackerPos
	if not tid then 
		Spring.Echo("Target is nil")
		return pos
	end
	if not Spring.ValidUnitID(TargetID) then	
		Spring.Echo("Invalid tarhetID --> "..tid)
	end
	
	if not Spring.GetUnitWeaponTestRange(aid,1,tid) then
		local range = Spring.GetUnitMaxRange(aid)
		local dist = Spring.GetUnitSeparation(aid,tid)
		local overRange = dist/range
		local travelRate = (overRange -1)*1.2 +0.1
		local tX, tY, tZ = Spring.GetUnitPosition(tid)		
		local targetPos = Vec3(tX,tY,tZ)
		
		
		local vecDiff = (targetPos-attackerPos)*travelRate
		pos = attackerPos + vecDiff
	end
	
	return pos 
end

