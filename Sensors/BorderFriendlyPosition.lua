local sensorInfo = {
	name = "BorderFriendlyPosition",
	desc = "Return position within map boundaries",
	author = "PepeAmpere",
	date = "2017-05-20",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- instant, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description return updated position within map boundaries based on the orginal position and the vector update 
-- @argument OriginalPosition [Vec3] - position to be updated by vector
-- @argument VectorUpdate [Vec3] - vector to update the position with regards to the map borders
return function(OriginalPosition, VectorUpdate)
	local originalPosition = OriginalPosition
	local vectorUpdate = VectorUpdate
	local maxX = Game.mapSizeX
	local maxZ = Game.mapSizeZ
	local returnPosition = Vec3(0,0,0)
	--core.Position(units[1])+Vec3(CommanderVelocity.dirX*50,0,CommanderVelocity.dirZ*50)
	if(originalPosition["x"]+vectorUpdate["x"] > maxX) then
		returnPosition["x"] = maxX
	else
		returnPosition["x"] = originalPosition["x"]+vectorUpdate["x"]
	end
	
	if(originalPosition["x"]+vectorUpdate["x"] < 0) then
		returnPosition["x"] = 0
	else
		returnPosition["x"] = originalPosition["x"]+vectorUpdate["x"]
	end
	
	if(originalPosition["z"]+vectorUpdate["z"] > maxZ) then
		returnPosition["z"] = maxZ
	else
		returnPosition["z"] = originalPosition["z"]+vectorUpdate["z"]
	end
	
	if(originalPosition["z"]+vectorUpdate["z"] < 0) then
		returnPosition["z"] = 0
	else
		returnPosition["z"] = originalPosition["z"]+vectorUpdate["z"]
	end
		
	return returnPosition
end