local sensorInfo = {
	name = "GetPath",
	desc = "Sends data to example debug widget",
	author = "PepeAmpere",
	date = "2018-04-16",
	license = "MIT",
}


local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
--local ID = 0
function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end
return function(unitsForRescue)
	
	local rescuee = nil
	for unitID,unitState in pairs(unitsForRescue) do
		if Spring.ValidUnitID(unitID) then	
			if unitState == "not_rescued" then 
				unitsForRescue[unitID] = "being_rescued"
				rescuee = unitID
				break
			end
		end
	
	end
	
	return rescuee
end