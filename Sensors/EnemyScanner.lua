local sensorInfo = {
	name = "DVN",
	desc = "Sends data to example debug widget",
	author = "PepeAmpere",
	date = "2018-04-16",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end
function file_exists(file)
  local f = io.open(file, "rb")
  if f then f:close() end
  return f ~= nil
end
function lines_from(file)
  if not file_exists(file) then return {"nope"} end
  lines = {}
  for line in io.lines(file) do 
    lines[#lines + 1] = line
  end
  return lines
end
-- @description return current wind statistics
return function()

	local stuff = lines_from("LuaUI/BETS/projects/nota_mastr_ukol1/cesta.txt")
	local m = #stuff / 2
	local cesta = {}
	local k = 1
	for i=1,m do
		cesta[i] = { x = stuff[k], z = stuff[k+1] }
		k = k+2
	end
	
	
	--local cesta = { { x = 77, z = 15 }, { x = 76, z = 16 }, { x = 75, z = 17 }, { x = 74, z = 18 }, { x = 73, z = 19 }, { x = 72, z = 20 }, { x = 71, z = 21 }, { x = 70, z = 22 }, { x = 69, z = 23 }, { x = 68, z = 24 }, { x = 67, z = 25 }, { x = 66, z = 26 }, { x = 65, z = 27 }, { x = 64, z = 28 }, { x = 63, z = 29 }, { x = 62, z = 30 }, { x = 61, z = 31 }, { x = 60, z = 32 }, { x = 59, z = 33 }, { x = 58, z = 34 }, { x = 57, z = 35 }, { x = 56, z = 36 }, { x = 55, z = 37 }, { x = 55, z = 38 }, { x = 56, z = 39 }, { x = 57, z = 40 }, { x = 58, z = 41 }, { x = 59, z = 42 }, { x = 60, z = 43 }, { x = 61, z = 44 }, { x = 62, z = 45 }, { x = 63, z = 46 }, { x = 64, z = 47 }, { x = 64, z = 48 }, { x = 63, z = 49 }, { x = 62, z = 50 }, { x = 61, z = 51 }, { x = 60, z = 52 }, { x = 59, z = 53 }, { x = 58, z = 54 }, { x = 57, z = 55 }, { x = 56, z = 56 }, { x = 55, z = 56 } }
	if #cesta > 0 then
		local ID = 0
		
		for i=1,#cesta-1 do
			ID = ID+1
			local x = cesta[i].x*50
		
			
			local z = cesta[i].z*50
			
			local y = Spring.GetGroundOrigHeight(x,z)
			local x2 = cesta[i+1].x*50
			local z2 = cesta[i+1].z*50
			local y2 = Spring.GetGroundOrigHeight(x2,z2)
			
			if (Script.LuaUI('exampleDebug_update')) then
				Script.LuaUI.exampleDebug_update(
					ID, -- key
					{	-- data
						startPos = Vec3(x,y,z), 
						endPos = Vec3(x2,y2,z2)
					}
				)
			end
		end
		return {	-- data
					startPos = Vec3(cesta[1].x*50,Spring.GetGroundOrigHeight(cesta[1].x*50,cesta[1].z*50),cesta[1].z*50), 
					endPos = Vec3(cesta[#cesta].x*50,Spring.GetGroundOrigHeight(cesta[#cesta].x*50,cesta[#cesta].z*50),cesta[#cesta].z*50),
					cesta = cesta,
					radky = stuff,
					m = m
				}
	end
end