local sensorInfo = {
	name = "GetPath",
	desc = "Sends data to example debug widget",
	author = "PepeAmpere",
	date = "2018-04-16",
	license = "MIT",
}


local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
--local ID = 0
function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end
function ipm(x,z,xMax)
	return (z-1)*xMax + x
end
function g2p(path)
	local rp = {}
	for i=1,#path do
		rp[i] = Vec3(path[i].x*50,Spring.GetGroundOrigHeight(path[i].x*50,path[i].z*50),path[i].z*50)
	end
	return rp

end
function positionToGridPoint(pos,gridStep)
	local baseX = pos.x
	local baseZ = pos.z
	
	local retX = (( baseX - ( baseX % gridStep ) ) / gridStep ) + 1
	local retZ = (( baseZ - ( baseZ % gridStep ) ) / gridStep ) + 1
	
	
	return Vec3(retX,0,retZ)  --{retX,retZ}

end
-- @description return current wind statistics
return function(mapaCest,finish,maxX,gridStep,zpatky)
	local cil = positionToGridPoint(finish, gridStep)
	local finishX = cil.x
	local finishZ = cil.z
	local predchozi = mapaCest[ipm(finishX,finishZ,maxX)].cesta
	local totdis = mapaCest[ipm(finishX,finishZ,maxX)].delka
	local returnPath = {}
	bb.drawdebug = mapaCest[ipm(finishX,finishZ,maxX)]
	
	

	if not zpatky then
		--bb.pth = {}
		--bb.pth[totdis+1] = Vec3(mapaCest[ipm(finishX,finishZ,maxX)].x,mapaCest[ipm(finishX,finishZ,maxX)].danger,mapaCest[ipm(finishX,finishZ,maxX)].z)
		returnPath[totdis+1] = Vec3(mapaCest[ipm(finishX,finishZ,maxX)].x,mapaCest[ipm(finishX,finishZ,maxX)].danger,mapaCest[ipm(finishX,finishZ,maxX)].z)
		for c=1,totdis do
			--bb.pth[totdis-c+1] = Vec3(mapaCest[predchozi].x,mapaCest[predchozi].danger,mapaCest[predchozi].z)
			returnPath[totdis-c+1] = Vec3(mapaCest[predchozi].x,mapaCest[predchozi].danger,mapaCest[predchozi].z)
			predchozi = mapaCest[predchozi].cesta
			
		end
	
	else
		--bb.pth = {}
		--bb.pth[totdis+1] = Vec3(mapaCest[ipm(finishX,finishZ,maxX)].x,mapaCest[ipm(finishX,finishZ,maxX)].danger,mapaCest[ipm(finishX,finishZ,maxX)].z)
		returnPath[1] = Vec3(mapaCest[ipm(finishX,finishZ,maxX)].x,mapaCest[ipm(finishX,finishZ,maxX)].danger,mapaCest[ipm(finishX,finishZ,maxX)].z)
		for c=1,totdis do
			--bb.pth[c+1] = Vec3(mapaCest[predchozi].x,mapaCest[predchozi].danger,mapaCest[predchozi].z)
			returnPath[c+1] = Vec3(mapaCest[predchozi].x,mapaCest[predchozi].danger,mapaCest[predchozi].z)
			predchozi = mapaCest[predchozi].cesta
			
		end
	
	end
	
	
	
	
	return g2p(returnPath)
	
end 