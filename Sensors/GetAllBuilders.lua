local sensorInfo = {
	name = "GetAllBuilders",
	desc = "Vrati jednotky spravneho typu ze seznamu",
	author = "PepeAmpere",
	date = "2018-04-16",
	license = "MIT",
}


local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
--local ID = 0
function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end
return function()
	
	local matchingUnits = {}
	if not global.AllUnits then
		Logger.Warn("No units available")
		return matchingUnits
	end
	
	for unitID,vlastnosti in pairs(global.AllUnits) do
		if vlastnosti.defID == 61 then 
			matchingUnits[unitID] = vlastnosti
		end
	end
	return matchingUnits
end