local sensorInfo = {
	name = "CreateGroup",
	desc = "Takes a list of unit IDs and transforms it into map unit ID -> position index",
	author = "PepeAmpere",
	date = "2017-04-22",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description Take passed positons, do some transformations over them and return the result back
-- @argument SourceList [array of UnitID] list of unit IDs representing group   
-- @argument SourceOrder [array of number|optional] list of positions for desired group 
-- @argument SecondaryList [array of UnitID|optional] list of secondary group that will get affixed after the source group if not given specific order within the new group
-- @argument SecondaryOrder [array of number|optional] list of positions for the secondary group. If not passed, SecondaryList shall occupy positions #SourceList+1..#SourceList+1+#SecondaryList
-- @argument TotalOrder [array of number|optional] list of positions for all units combined, mapping SourceList,SecondaryList to TotalOrder
return function(SourceList, SourceOrder, SecondaryList, SecondaryOrder, TotalOrder)
	--Spring.Echo("CREATE GROUP DEBUG")
	--Spring.Echo(dump(SourceList))
	--Spring.Echo(dump(SourceOrder))
	--Spring.Echo(dump(SecondaryList))
	--Spring.Echo(dump(SecondaryOrder))

	--return SourceList
	
	
	
	local sourceList = SourceList
	local sourceOrder = SourceOrder
	local secondaryList = SecondaryList
	local secondaryOrder = SecondaryOrder
	local totalOrder = TotalOrder
	local finalGroup = {}
	-- check bad inputs
	if (TotalOrder == nil) then 
		--kontrola primary listu 
		if(SourceOrder == nil) then
			--default positioning 
			sourceOrder = {}
			for i=1,#SourceList do
				sourceOrder[i] = i
			end	
		else
			--jedeme podle source orderu
			if(#SourceList ~= #SourceOrder) then
				Logger.warn("Dimensions of SourceList and SourceOrder do not match!")
			end			
		end
		--kontrola secondary listu
		if(SecondaryList == nil) then
			-- ok
		else
			if(SecondaryOrder == nil) then
				--default positioning secondary listu
				secondaryOrder = {}
				for i=1,#SecondaryList do
					secondaryOrder[i] = i+#sourceList
				end
			else
				if(#SecondaryList ~= #SecondaryOrder) then
					Logger.warn("Dimensions of SecondaryList and SecondaryOrder do not match!")
				end
			end
		end
	else
		if(SecondaryList == nil) then
			--SL = TO
			if(#SourceList ~= #TotalOrder) then 
				Logger.warn("Dimensions of SourceList and TotalOrder do not match!")
			end
		else
			--SL + SL = TL
			if(#SourceList + #SecondaryList ~= #TotalOrder) then
				Logger.warn("Dimensions of SourceList+SecondaryList and TotalOrder do not match!")
			end
		end
	end
	--do the meth
	if(totalOrder == nil) then		
		for h=1,#sourceList do
			finalGroup[sourceList[h]] = sourceOrder[h]
		end
		if(secondaryList ~= nil) then
			for h=1,#secondaryList do
				finalGroup[secondaryList[h]] = secondaryOrder[h]
			end
		end
	else
		for h=1,#sourceList do
			finalGroup[sourceList[h]] = totalOrder[h]
		end
		if(secondaryList ~= nil) then
			for h=1,#secondaryList do
				finalGroup[secondaryList[h]] = totalOrder[h+#sourceList]
			end
		end
	end
	return finalGroup
	--return SourceList
end