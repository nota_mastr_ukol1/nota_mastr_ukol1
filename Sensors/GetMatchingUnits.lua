local sensorInfo = {
	name = "GetMatchingUnits",
	desc = "Vrati jednotky spravneho typu ze seznamu",
	author = "PepeAmpere",
	date = "2018-04-16",
	license = "MIT",
}


local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
--local ID = 0
function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end
return function(kriteria)
	
	local matchingUnits = {}
	if not global.AllUnits then
		Logger.Warn("No order available")
		return matchingUnits
	end
	for unitID,vlastnosti in pairs(global.AllUnits) do
		local skipni = false
		for nazev,hodnota in pairs(kriteria) do
			if (not vlastnosti[nazev]) or vlastnosti[nazev] ~= hodnota then
				skipni = true
			end			
		end
		if not skipni then 
			matchingUnits[unitID] = vlastnosti
		end
	end
	return matchingUnits
end