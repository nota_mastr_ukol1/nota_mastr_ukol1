local sensorInfo = {
	name = "UpdateGlobalUnits",
	desc = "Updates list of our units and their designation",
	author = "PepeAmpere",
	date = "2018-04-16",
	license = "MIT",
}


local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
--local ID = 0
function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end
return function()
	
	if not global.AllUnits then
		global.AllUnits = {}
	end
	
	local currentUnits = Spring.GetTeamUnits(Spring.GetMyTeamID() )
	local currentUnitsRev = {}
	local unitsDiscovered = 0
	for count,unitID in pairs(currentUnits) do
		currentUnitsRev[unitID] = { designation = "new", defID = Spring.GetUnitDefID(unitID), stav = "waiting", name = UnitDefs[Spring.GetUnitDefID(unitID)].name, squadID = 0}
	end
	--clean up the global.AllUnits
	for unitID,designation in pairs(global.AllUnits) do
		if not Spring.ValidUnitID(unitID) then
			--we remove invalid unit from our global.AllUnits
			global.AllUnits[unitID] = nil
		else
			--tuhle jednotku jsme již zprocesovali
			currentUnitsRev[unitID] = nil
		end
	
	end
	
	
	for unitID,designation in pairs(currentUnitsRev) do
		unitsDiscovered = unitsDiscovered + 1
		global.AllUnits[unitID] = designation
	
	end
	return unitsDiscovered
end