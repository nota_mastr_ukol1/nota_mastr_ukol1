local sensorInfo = {
	name = "SafePathPlanner",
	desc = "Plans the safest path on a danger map.",
	author = "PepeAmpere",
	date = "2017-05-20",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- instant, no caching

local GRID_STEP = 50 --how big a grid step is, for speedup, really 

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

function positionToGridPoint(pos,gridStep)
	local baseX = pos.x
	local baseZ = pos.z
	
	local retX = (( baseX - ( baseX % gridStep ) ) / gridStep ) + 1
	local retZ = (( baseZ - ( baseZ % gridStep ) ) / gridStep ) + 1
	
	
	return {retX,retZ}

end

function getNeighbours(xi,zi,grid)
	local retNeighbours = {}
	
	for i=-1,1 do
		for j=-1,1 do			
			local x = xi+i
			local z = zi+j
			if (x >= 1) and (z >= 1) and (x <= #grid) and (z <= #grid[1]) and ((x ~= xi) or (z ~=zi)) then
				retNeighbours[#retNeighbours + 1] = {x = x,z = z}
			end
		end
	end
	
	return retNeighbours
end

function drawAllPaths(mapaCest)
	for u=1,#mapaCest do
		for v=1,#mapaCest[u] do
			if mapaCest[u][v].delka > 0 then 
				Sensors.nota_mastr_ukol1.DrawSafePath(mapaCest[u][v].cesta)
			end
		end	
	end
end
function udelejMapu(grid)
	local cestomapa = {}
	for x=1,#grid do
		cestomapa[x] = {}
		for z=1,#grid[x] do
			cestomapa[x][z] = {}
			cestomapa[x][z]["delka"] = -1
			cestomapa[x][z]["danger"] = grid[x][z]
			cestomapa[x][z]["dangerSum"] = grid[x][z]
			cestomapa[x][z]["cesta"] = {}
			cestomapa[x][z]["fronta"] = false
		end
	end
	return cestomapa
end
function updatniCestu(novaCesta)
	local newpath = {}
	for i=1,#novaCesta do
		newpath[i] = {x =  novaCesta[i].x, z = novaCesta[i].z }	
	end
	return newpath

end
function file_exists(file)
  local f = io.open(file, "rb")
  if f then f:close() end
  return f ~= nil
end
function lines_from(file)
  if not file_exists(file) then return {"nope"} end
  lines = {}
  for line in io.lines(file) do 
    lines[#lines + 1] = line
  end
  return lines
end
function zapis_mapu(mapa,file,xmax,zmax)
	local ff = io.open(file, "w")
	ff:write(xmax .. "\n")
	ff:write(zmax .. "\n")
	for i=1,xmax do
		for j=1,zmax do
			ff:write(mapa[i][j] .. "\n")
		end
	end
		
	ff:close()


end
function zapis_danger(mapa,file,xmax,zmax)
	local ff = io.open(file, "w")
	ff:write(xmax .. "\n")
	ff:write(zmax .. "\n")
	for i=1,xmax do
		for j=1,zmax do
			ff:write("["..i..","..j.."]: "..mapa[i][j].dangerSum .. "\n")
		end
	end
		
	ff:close()


end
-- @description scan whole map for hills of certain height and return them in a list {minX,minZ,x,z,maxX,maxZ} 
--				where minXZ are the top left corner and maxXZ are the bottom right corner of the hill
-- @argument gridSize - default 50 - the granularity of the scan 
-- @argument gridBased - default true - whether the return table is indexed by grid size or by amount of elements 
return function(DangerGrid,pos1,pos2,gridStep)
	local returnPath = {}	
	local dangerGrid = DangerGrid
	local maxX = #dangerGrid
	local start = positionToGridPoint(pos1,gridStep)
	local finish = positionToGridPoint(pos2, gridStep)
	local startX = start[1]
	local startZ = start[2]
	local finishX = finish[1]
	local finishZ = finish[2]
	bb.sx = start
	bb.dg = dangerGrid
	bb.nb = getNeighbours(startX,startZ,dangerGrid)
	bb.dangerMin = math.max(dangerGrid[startX][startZ],dangerGrid[finishX][finishZ])
	--check jestli jsme bez cesty co hledat
	if startX == finishX and startZ == finishZ then return returnPath end
	
	--nastavim dangerMin dle startu a cile
	--generujeme frontu okoli
	--pridame do mapy cest ty, ktere jsou 
	--	-kratsi
	--	-bezpecnejsi
	local mapaCest = {} -- mapaCest[x][z] = {delka,danger}
	mapaCest = udelejMapu(dangerGrid)
	bb.dgt = dangerGrid[81][140]
	bb.mct = mapaCest[81][140].danger
	bb.mc = mapaCest
	local fronta = {}
	local fronta2 = {}
	local origFronta = {}
	origFronta[1] = {x = startX,z = startZ}
	mapaCest[startX][startZ].delka = 0
	mapaCest[startX][startZ].cesta[1] = {x = startX,z = startZ}
	mapaCest[startX][startZ].fronta = true
	bb.logger = {}
	Spring.Echo("search initialised")
	local loops = 0
	while true do
		Spring.Echo("loop start")
		loops = loops +1
		--Spring.Echo(#origFronta)
		local nasbod
			for n,o in ipairs(origFronta) do 
				nasbod = o
				fronta = getNeighbours(nasbod.x,nasbod.z,dangerGrid)
				tododelka = mapaCest[nasbod.x][nasbod.z]["delka"]
				tododanger = mapaCest[nasbod.x][nasbod.z]["danger"]
				mapaCest[nasbod.x][nasbod.z].fronta = false
				--todocesta = mapaCest[nasbod.x][nasbod.z].cesta
				--bb.logger[#bb.logger + 1] = nasbod
				for k,i in ipairs(fronta) do
					-- i je dalsi bod cesty
					local potdanger = math.max(tododanger,dangerGrid[i.x][i.z])
					if mapaCest[i.x][i.z].delka < 0 then
						--bod nebyl navstiven, pridavame
						mapaCest[i.x][i.z].delka = tododelka+1 
						mapaCest[i.x][i.z].danger = potdanger
						fronta2[#fronta2 +1 ] = i 
						mapaCest[i.x][i.z].fronta = true
						--Spring.Echo("adding to fronta2")
						--Spring.Echo(dump(i))			
						mapaCest[i.x][i.z].cesta =  updatniCestu(mapaCest[nasbod.x][nasbod.z].cesta) --todocesta
						mapaCest[i.x][i.z].cesta[#mapaCest[i.x][i.z].cesta + 1] = {x = i.x, z = i.z}
						mapaCest[i.x][i.z].dangerSum = dangerGrid[i.x][i.z] + mapaCest[nasbod.x][nasbod.z].dangerSum
						--musime zkontrolovat jeho okoli 
						--add to fronta2
					else
						if mapaCest[i.x][i.z].danger >= potdanger and  mapaCest[i.x][i.z].dangerSum > dangerGrid[i.x][i.z] + mapaCest[nasbod.x][nasbod.z].dangerSum then
							--byli jsme navstiveni, ale nova je bezpecnejsi
							Spring.Echo("processing " .. dump(i))
							mapaCest[i.x][i.z].delka = tododelka+1 
							mapaCest[i.x][i.z].danger = potdanger
							mapaCest[i.x][i.z].cesta = {}
							--local tmpath = mapaCest[nasbod.x][nasbod.z].cesta
							mapaCest[i.x][i.z].cesta = updatniCestu(mapaCest[nasbod.x][nasbod.z].cesta) --todocesta
							mapaCest[i.x][i.z].cesta[#mapaCest[i.x][i.z].cesta + 1] = {x = i.x, z = i.z} 
							mapaCest[i.x][i.z].dangerSum = dangerGrid[i.x][i.z] + mapaCest[nasbod.x][nasbod.z].dangerSum
							if not mapaCest[i.x][i.z].fronta then 
								fronta2[#fronta2 +1 ] = i 
								mapaCest[i.x][i.z].fronta = true
							end
						end
					end
					-- if mapaCest[i.x][i.z].delka < 0 -> mapaCest[i.x][i.z].delka = tododelka+1 && mapaCest[i.x][i.z].danger = max(mapaCest[i.x][i.z].danger, tododanger)
					-- else
					--  uz bylo navstiveno
					--  if mapaCest[i.x][i.z].danger < tododanger then 
					--		
				end
				--nyni mame ve fronte2 ulozene dalsi body, ktere se menily
				bb.f2 = fronta2
			end 
		--or loops > 50
		if #fronta2 == 0 or loops > 100  then 
			Spring.Echo("Lloop end")
			--os.execute("start cmd.exe")
			Spring.Echo(os.clock())
			bb.txt = lines_from("LuaUI/BETS/projects/nota_mastr_ukol1/test.txt")
			zapis_mapu(dangerGrid,"LuaUI/BETS/projects/nota_mastr_ukol1/findme2.txt",#dangerGrid,#dangerGrid[1])
			zapis_danger(mapaCest,"LuaUI/BETS/projects/nota_mastr_ukol1/dangers.txt",#dangerGrid,#dangerGrid[1])
			Spring.Echo("write done")
			break
		else
			--Spring.Echo("fronta 2")
			--Spring.Echo(dump(fronta2))
			--Spring.Echo("origFronta")
			--Spring.Echo(dump(origFronta))
			--Sensors.nota_mastr_ukol1.DrawSafePath(fronta2)
			origFronta = fronta2
			fronta2 = {}
			fronta = {}
			todocesta = {}
			--Spring.Echo("fronta 2")
			--Spring.Echo(dump(fronta2))
			--Spring.Echo("origFronta")
			--Spring.Echo(dump(origFronta))
		end
	
	end
	--returnPath[1] = start
	--returnPath[2]= finish
	returnPath = mapaCest[finishX][finishZ].cesta
	
	
	return returnPath
end


