local sensorInfo = {
	name = "ChciBuildery",
	desc = "Vrati zda chci buildery",
	author = "PepeAmpere",
	date = "2018-04-16",
	license = "MIT",
}


local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
--local ID = 0
function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

function upravNakup(nakup,typy,objednavka)
	
	if objednavka.typ == "basic" then 
		for i,typ in pairs(typy) do
			if typ == objednavka.jmeno then
				nakup[i] = nakup[i] - objednavka.pocet
				if nakup[i] < 0 then 
					Logger.warn("negative purchase detected!"..nakup[i])
				end
				break
			end
		
		end
	end
	if objednavka.typ == "combined" then
		for i,ob in ipairs(objednavka) do
			nakup = upravNakup(nakup,typy,ob)
		end
	
	end


	return nakup
	
end
return function(potreby, typy ,vlastnictvi,mojeMinuleObjednavky)
	
	Sensors.nota_mastr_ukol1.UpdateGlobalUnits()
	
	--TODO FIX THIS SHIT
		local bds = Sensors.nota_mastr_ukol1.GetMatchingUnits({ designation = "builder"}) 
		local bdc = Sensors.nota_mastr_ukol1.GetCount(bds)
		local abds = Sensors.nota_mastr_ukol1.GetAllBuilders()
		local CBDS = Sensors.nota_mastr_ukol1.ClaimXBuilders(abds,4-bdc)
		local bds2 = Sensors.nota_mastr_ukol1.GetMatchingUnits({ designation = "builder"})
		local bdc2 = Sensors.nota_mastr_ukol1.GetCount(bds2)
		if vlastnictvi[1] ~= bdc2 then
			Spring.Echo("mismatched builder count"..vlastnictvi[1]..bdc2)
			vlastnictvi[1] = bdc2
		end
	
	--
	local nakup = {}
	for i=1,#potreby do
		nakup[i] = potreby[i] - vlastnictvi[i]
	end
	if not mojeMinuleObjednavky then	
		--Logger.warn("zadne minule objednavky")
		Spring.Echo("zadne minule objednavky")
		return nakup --[[{nakup = nakup,
			potreby = potreby,
			objs = mojeMinuleObjednavky}]]--
	end
	
	for ID,objednavka in pairs(mojeMinuleObjednavky) do
		objednavka.stav = "vyrizena"
	end
	
	for i,objednavka in pairs(global.nakupni_seznam) do 
		if mojeMinuleObjednavky[objednavka.UID] then 
			--moje objednavka je ve fronte
			mojeMinuleObjednavky[objednavka.UID].stav = "fronta"
			
		end	
	end
	
	if global.prave_vyrizovana_objednavka then 
		if mojeMinuleObjednavky[global.prave_vyrizovana_objednavka.UID] then
			mojeMinuleObjednavky[global.prave_vyrizovana_objednavka.UID].stav = "fronta"
			--celkemObjednano = celkemObjednano + global.prave_vyrizovana_objednavka.pocet 
		end
	end	
	
	for ID,objednavka in pairs(mojeMinuleObjednavky) do
		if objednavka.stav == "fronta" then
		--tato je ve fronte
		nakup = upravNakup(nakup,typy,objednavka)
		--Spring.Echo("test")
		end
	end
	
	--[[
	for i=1,#potreby do
		
		local kolikChci = potreby[i]
		local kolikMam = vlastnictvi[i]
		local typ = typy[i]
		
		local celkemObjednano = 0
	
			
		
		
		
		
		local pocet = 0
		if not seznam then return pocet end 
		for i,j in pairs(seznam) do 
			pocet = pocet +1
		end
		
	end]]--
	
	
	return nakup --[[{nakup = nakup,
			potreby = potreby,
			objs = mojeMinuleObjednavky}]]--
	
end