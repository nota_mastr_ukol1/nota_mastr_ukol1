local sensorInfo = {
	name = "FindNewUnit",
	desc = "Sends data to example debug widget",
	author = "PepeAmpere",
	date = "2018-04-16",
	license = "MIT",
}


local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
--local ID = 0
function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end
return function(oldUnits)
	
	local units = Spring.GetTeamUnits(Spring.GetMyTeamID() )
	local newUnitID = nil
	local finds = 0
	for index,unitID in pairs(units) do
		if not oldUnits[unitID] then 
			newUnitID = unitID
			finds = finds+1
		end
	end
	if finds == 0 then 	Logger.warn("No new unit detected") end
	if finds > 1 then Logger.warn("There were ".. finds .. " new units found.") end
	return newUnitID
end