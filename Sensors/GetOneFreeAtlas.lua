local sensorInfo = {
	name = "GetOneFreeAtlas",
	desc = "Sends data to example debug widget",
	author = "PepeAmpere",
	date = "2018-04-16",
	license = "MIT",
}


local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
--local ID = 0
function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end
return function(atlases)
	
	local atlas = nil
	for unitID,unitState in pairs(atlases) do
		if Spring.ValidUnitID(unitID) then	
			if unitState == "free" then 
				atlases[unitID] = "occupied"
				atlas = unitID
				break
			end
		end
	
	end
	
	return atlas
end