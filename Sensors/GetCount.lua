local sensorInfo = {
	name = "GetCount",
	desc = "Vrati pocet zaznamu seznamu",
	author = "PepeAmpere",
	date = "2018-04-16",
	license = "MIT",
}


local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
--local ID = 0
function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end
return function(seznam)
	
	local pocet = 0
	if not seznam then return pocet end 
	for i,j in pairs(seznam) do 
		pocet = pocet +1
	end
	return pocet
end