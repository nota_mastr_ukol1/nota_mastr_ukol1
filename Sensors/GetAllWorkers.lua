local sensorInfo = {
	name = "GetAllWorkers",
	desc = "Sends data to example debug widget",
	author = "PepeAmpere",
	date = "2018-04-16",
	license = "MIT",
}


local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
--local ID = 0
function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end
return function()
	
	local workers = Sensors.core.FilterUnitsByCategory(Spring.GetTeamUnits(Spring.GetMyTeamID() ),Categories.Common.builders)
	local atlasesRev = {}
	for index,unitID in pairs(workers) do
		atlasesRev[unitID] = "free"	
	end
	
	return { forID = workers,
			 byID = atlasesRev }
end