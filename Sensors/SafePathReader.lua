local sensorInfo = {
	name = "SafePathReader",
	desc = "Plans the safest path on a danger map.",
	author = "PepeAmpere",
	date = "2017-05-20",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- instant, no caching

local GRID_STEP = 50 --how big a grid step is, for speedup, really 

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

function file_exists(file)
  local f = io.open(file, "rb")
  if f then f:close() end
  return f ~= nil
end
function lines_from(file)
  if not file_exists(file) then return {"nope"} end
  lines = {}
  for line in io.lines(file) do 
    lines[#lines + 1] = line
  end
  return lines
end
-- @description scan whole map for hills of certain height and return them in a list {minX,minZ,x,z,maxX,maxZ} 
--				where minXZ are the top left corner and maxXZ are the bottom right corner of the hill
-- @argument gridSize - default 50 - the granularity of the scan 
-- @argument gridBased - default true - whether the return table is indexed by grid size or by amount of elements 
return function(filePaths,fileDesc)
	local grid = {}
	local radky = lines_from("LuaUI/BETS/projects/nota_mastr_ukol1/"..filePaths)
	local detaily = lines_from("LuaUI/BETS/projects/nota_mastr_ukol1/"..fileDesc)
	
	local k = 1
	local ukazatel = 1
	local ux =1
	local uz = 1
	local xMax = detaily[1]
	local zMax = detaily[2]
	
	for xx=1,xMax do
		grid[xx] = {}
		for zz=1,zMax do
			grid[xx][zz] = {}
			grid[xx][zz]["cesta"] = {}
		
		end
	end
	for i=3,#detaily do
		--delky jednotlivych cest
		local cesta = {}
		local cestaD = detaily[i]
		
		for j=ukazatel,ukazatel+cestaD-1 do
			cesta[j] = {x = radky[k], z = radky[k+1] }
			k = k+2
		end
		ukazatel = ukazatel+cestaD
		if (i-2) > xMax*ux then
			uz = uz +1
			ux = 1
		end
		grid[ux][uz]["cesta"] = cesta
		ux = ux + 1
	end
	
	
	return grid
end


