local sensorInfo = {
	name = "GetBuilders",
	desc = "Sends data to example debug widget",
	author = "PepeAmpere",
	date = "2018-04-16",
	license = "MIT",
}


local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
--local ID = 0
function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end
return function(workers)
	
	builders = {}
	
	for unitID,designation in pairs(workers) do
		if designation == "builder" then
			builders[#builders] = unitID
			buildersRev[unitID] = "builder"
		end
	end
	
	return { forID = builders,
			 byID = buildersRev }
end