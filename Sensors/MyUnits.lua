local sensorInfo = {
	name = "MyUnits",
	desc = "Sends data to example debug widget",
	author = "PepeAmpere",
	date = "2018-04-16",
	license = "MIT",
}


local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
--local ID = 0
function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end
return function()
	
	local units = Spring.GetTeamUnits(Spring.GetMyTeamID() )
	local unitsRev = {}
	for index,unitID in pairs(units) do
		unitsRev[unitID] = "free"	
	end
	
	return { forID = units,
			 byID = unitsRev }
end