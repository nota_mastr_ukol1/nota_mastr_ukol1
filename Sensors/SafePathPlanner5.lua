local sensorInfo = {
	name = "SafePathPlanner",
	desc = "Plans the safest path on a danger map.",
	author = "PepeAmpere",
	date = "2017-05-20",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- instant, no caching

local GRID_STEP = 50 --how big a grid step is, for speedup, really 

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

function positionToGridPoint(pos,gridStep)
	local baseX = pos.x
	local baseZ = pos.z
	
	local retX = (( baseX - ( baseX % gridStep ) ) / gridStep ) + 1
	local retZ = (( baseZ - ( baseZ % gridStep ) ) / gridStep ) + 1
	
	
	return Vec3(retX,0,retZ)  --{retX,retZ}

end

function getSquareNeighbours(xi,zi,xMax,r)
	local retNeighbours = {}
	
	for i=-r,r do
		for j=-r,r do			
			local x = xi+i
			local z = zi+j
			if (x >= 1) and (z >= 1) and (x <= xMax) and (z <= xMax) and ((x ~= xi) or (z ~=zi)) then
				retNeighbours[#retNeighbours + 1] = ipm(x,z,xMax)
			end
		end
	end
	
	return retNeighbours


end

function getNeighbours(xi,zi,xMax)
	local retNeighbours = {}
	
	for i=-1,1 do
		for j=-1,1 do			
			local x = xi+i
			local z = zi+j
			if (x >= 1) and (z >= 1) and (x <= xMax) and (z <= xMax) and ((x ~= xi) or (z ~=zi)) then
				retNeighbours[#retNeighbours + 1] = ipm(x,z,xMax)
			end
		end
	end
	
	return retNeighbours
end

function drawAllPaths(mapaCest)
	for u=1,#mapaCest do
		for v=1,#mapaCest[u] do
			if mapaCest[u][v].delka > 0 then 
				Sensors.nota_mastr_ukol1.DrawSafePath(mapaCest[u][v].cesta)
			end
		end	
	end
end

function ipm(x,z,xMax)
	return (z-1)*xMax + x
end
function udelejMapu(grid,maxX)
	local cestomapa = {}
	for x=1,maxX do
		for z=1,maxX do
			local ind = ipm(x,z,maxX)
			cestomapa[ind] = {
								x = x,
								z = z,
								cesta = ind,
								danger = grid[ind],
								dangerSum = grid[ind],
								fronta = false,
								delka = -1
        }
		end
	end
	return cestomapa
end
function g2p(path)
	local rp = {}
	for i=1,#path do
		rp[i] = Vec3(path[i].x*50,Spring.GetGroundOrigHeight(path[i].x*50,path[i].z*50),path[i].z*50)
	end
	return rp

end

-- @description scan whole map for hills of certain height and return them in a list {minX,minZ,x,z,maxX,maxZ} 
--				where minXZ are the top left corner and maxXZ are the bottom right corner of the hill
-- @argument gridSize - default 50 - the granularity of the scan 
-- @argument gridBased - default true - whether the return table is indexed by grid size or by amount of elements 
return function(DangerGrid,pos1,gridStep,MaxX)
	
	local dangerGrid = DangerGrid
	local mapaCest = {}
	local maxX = MaxX
	
	mapaCest = udelejMapu(dangerGrid,maxX)
	Sensors.nota_mastr_ukol1.DangerUpdater()
	local enemyUnits = global.allEnemies
	
	for id,po in pairs(enemyUnits) do
		local gridPoint = positionToGridPoint(po,gridStep)
		--bb.egp = gridPoint
		
		local dangerOkoli = getNeighbours(gridPoint.x,gridPoint.z,maxX)
		local dangerOkoli = getSquareNeighbours(gridPoint.x,gridPoint.z,maxX,5)
		
		--bb.edo = dangerOkoli
		--mapaCest[ipm(gridPoint.x,gridPoint.z,maxX)].danger = mapaCest[ipm(gridPoint.x,gridPoint.z,maxX)].danger + 5000
		dangerGrid[ipm(gridPoint.x,gridPoint.z,maxX)] = dangerGrid[ipm(gridPoint.x,gridPoint.z,maxX)] + 5000
		for a,b in ipairs(dangerOkoli) do
			--mapaCest[b].danger = mapaCest[b].danger + 500
			dangerGrid[b] = dangerGrid[b]+500
		end
	
	
	end
	


	local returnPath = {}	
	
	local start = positionToGridPoint(pos1,gridStep)
	local startX = start.x --[1]
	local startZ = start.z  --[2]
	bb.sx = start
	bb.dg = dangerGrid
	bb.nb = getNeighbours(startX,startZ,maxX)
	local tmpi = ipm(startX,startZ,maxX)
	
	bb.dgt = dangerGrid[tmpi]
	bb.mct = mapaCest[tmpi].danger
	bb.mc = mapaCest
	local fronta = {}
	local fronta2 = {}
	local origFronta = {}
	origFronta[1] =  ipm(startX,startZ,maxX) --Vec3(startX,0,startZ)
	--bb.bod = origFronta[1].x
	mapaCest[ipm(startX,startZ,maxX)].delka = 0
	mapaCest[ipm(startX,startZ,maxX)].cesta = ipm(startX,startZ,maxX)
	mapaCest[ipm(startX,startZ,maxX)].fronta = true
	bb.logger = {}
	bb.neighs = getNeighbours(mapaCest[ipm(startX,startZ,maxX)].x,mapaCest[ipm(startX,startZ,maxX)].z,maxX)
	--Spring.Echo("search initialised")
	--Spring.Echo(os.clock())
	local loops = 0
	local changed = true
	while changed do
		loops = loops +1
		changed = false
		
		--local nasbod
			for n,o in ipairs(origFronta) do 
				--nasbod = o
				--bb.bod = nasbod
				
				--local bodi = ipm(nasbod.x,nasbod.z,maxX)
				local bodi = o
				bb.bod = bodi
				--fronta = getNeighbours(nasbod.x,nasbod.z,maxX)
				fronta = getNeighbours(mapaCest[bodi].x,mapaCest[bodi].z,maxX)
				tododelka = mapaCest[bodi].delka
				tododanger = mapaCest[bodi].danger
				mapaCest[bodi].fronta = false
				--todocesta = mapaCest[nasbod.x][nasbod.z].cesta
				--bb.logger[#bb.logger + 1] = nasbod
				for k,i in ipairs(fronta) do
					-- i je dalsi bod cesty
					local potdanger = math.max(tododanger,dangerGrid[i])
					if mapaCest[i].delka < 0 then
						--bod nebyl navstiven, pridavame
						changed = true
						mapaCest[i].delka = tododelka+1 
						mapaCest[i].danger = potdanger
						fronta2[#fronta2 +1 ] = i 
						mapaCest[i].fronta = true
						--Spring.Echo("adding to fronta2")
						--Spring.Echo(dump(i))			
						mapaCest[i].cesta =  bodi
						mapaCest[i].dangerSum = dangerGrid[i] + mapaCest[bodi].dangerSum 
						--musime zkontrolovat jeho okoli 
						--add to fronta2
					else
						if mapaCest[i].danger >= potdanger and  mapaCest[i].dangerSum > dangerGrid[i] + mapaCest[bodi].dangerSum  then
							--byli jsme navstiveni, ale nova je bezpecnejsi
							--Spring.Echo("processing " .. dump(i))
							changed = true
							mapaCest[i].delka = tododelka+1 
							mapaCest[i].danger = potdanger
							mapaCest[i].cesta = bodi
							mapaCest[i].dangerSum = dangerGrid[i] + mapaCest[bodi].dangerSum 
							if not mapaCest[i].fronta then 
								fronta2[#fronta2 +1 ] = i 
								mapaCest[i].fronta = true
							end
						end
					end
				end
				--nyni mame ve fronte2 ulozene dalsi body, ktere se menily
				bb.f2 = fronta2
			end 
		--or loops > 50or loops > 100
		if #fronta2 == 0   then 
			--Spring.Echo("Lloop end"..loops)
			--os.execute("start cmd.exe")
			--Spring.Echo(os.clock())
			--Spring.Echo("write done")
			global.loopstotal = loops
			--break
		else
			origFronta = fronta2
			fronta2 = {}
			fronta = {}
			todocesta = {}
		end
	
	end

	
	return {
			start = mapaCest[ipm(startX,startZ,maxX)],
			si = ipm(startX,startZ,maxX),
			mp = mapaCest --g2p(returnPath)
			}
end


