local sensorInfo = {
	name = "HeightScanner2",
	desc = "Scans map for height",
	author = "PepeAmpere",
	date = "2017-05-20",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- instant, no caching

local GRID_STEP = 50 --how big a grid step is, for speedup, really 

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end
-- @description scan whole map for hills of certain height and return them in a list {minX,minZ,x,z,maxX,maxZ} 
--				where minXZ are the top left corner and maxXZ are the bottom right corner of the hill
-- @argument gridSize - default 50 - the granularity of the scan 
-- @argument gridBased - default true - whether the return table is indexed by grid size or by amount of elements 
return function(GridSize)
	local returnHeights = {}
	local gridSize = GridSize or 50
	local maxX = Game.mapSizeX
	local maxZ = Game.mapSizeZ
	local maxout = 0
	for z = 0,maxX,gridSize do
		for x = 0,maxZ,gridSize do
			--iterujeme po sloupcích a ulozime vysku 
			local tmpd = Spring.GetGroundOrigHeight(x,z)
			if tmpd < 0 then tmpd = 10 end
			if tmpd > 700 then tmpd = 50000 end
			returnHeights[#returnHeights+1] = tmpd		
			
		end	
		maxout = maxout + 1
	end
	global.maxX = maxout
	bb.maxX = maxout
	bb.gs = #returnHeights
	return returnHeights
end