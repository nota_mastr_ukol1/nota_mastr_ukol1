
local sensorInfo = {
	name = "GiveMoveOrder",
	desc = "Poda",
	author = "PepeAmpere",
	date = "2018-04-16",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
--local ID = 0
function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

function vecDist(v1,v2)

	return math.sqrt(((v1.x - v2.x)*(v1.x - v2.x)+ (v1.z - v2.z)*(v1.z - v2.z)))

end

return function(AttackerID,TargetID)
	
	local aid = AttackerID
	local tid = TargetID
	
	TargetIsAPosition = TargetIsAPosition or false
	--Spring.Echo(dump(tid))
	--Spring.Echo(tid.x)
	if not Spring.ValidUnitID(aid) then	
		Spring.Echo("Invalid AttackerID --> "..aid)
	end
	--if not Spring.ValidUnitID(TargetID) then	
	--	Spring.Echo("Invalid targetID --> "..tid)
	--end
	local cmdID = CMD.MOVE
	Spring.GiveOrderToUnit(aid, cmdID, {tid.x,tid.y,tid.z}, {"right"})
	return cmdID
end

