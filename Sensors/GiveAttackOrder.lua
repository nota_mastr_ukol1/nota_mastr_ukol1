
local sensorInfo = {
	name = "GiveAttackOrder",
	desc = "Poda",
	author = "PepeAmpere",
	date = "2018-04-16",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
--local ID = 0
function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

function vecDist(v1,v2)

	return math.sqrt(((v1.x - v2.x)*(v1.x - v2.x)+ (v1.z - v2.z)*(v1.z - v2.z)))

end

return function(AttackerID,TargetID)
	
	local aid = AttackerID
	local tid = TargetID
	
	TargetIsAPosition = TargetIsAPosition or false
	
	if not Spring.ValidUnitID(aid) then	
		Spring.Echo("Invalid AttackerID --> "..aid)
	end
	if not tid then
		Spring.Echo("Target is nil")
		return nil
	end
	if not Spring.ValidUnitID(TargetID) then	
		Spring.Echo("Invalid targetID --> "..tid)
	end
	local cmdID = CMD.ATTACK
	Spring.GiveOrderToUnit(aid, cmdID, {tid}, {"right"})
	return cmdID
end

