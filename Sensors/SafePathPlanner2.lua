local sensorInfo = {
	name = "SafePathPlanner2",
	desc = "Plans the safest path on a danger map.",
	author = "PepeAmpere",
	date = "2017-05-20",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- instant, no caching

local GRID_STEP = 50 --how big a grid step is, for speedup, really 

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

function positionToGridPoint(pos,gridStep)
	local baseX = pos.x
	local baseZ = pos.z
	
	local retX = (( baseX - ( baseX % gridStep ) ) / gridStep ) + 1
	local retZ = (( baseZ - ( baseZ % gridStep ) ) / gridStep ) + 1
	
	
	return {retX,retZ}

end

function getNeighbours(xi,zi,grid)
	local retNeighbours = {}
	
	for i=-1,1 do
		for j=-1,1 do			
			local x = xi+i
			local z = zi+j
			if (x >= 1) and (z >= 1) and (x <= #grid) and (z <= #grid[1]) and ((x ~= xi) or (z ~=zi)) then
				retNeighbours[#retNeighbours + 1] = {x = x,z = z}
			end
		end
	end
	
	return retNeighbours
end

function udelejMapu(grid)
	local cestomapa = {}
	for x=1,#grid do
		cestomapa[x] = {}
		for z=1,#grid[x] do
			cestomapa[x][z] = {}
			cestomapa[x][z]["delka"] = -1
			cestomapa[x][z]["danger"] = grid[x][z]
			cestomapa[x][z]["dangerSum"] = grid[x][z]
			cestomapa[x][z]["cesta"] = {}
			cestomapa[x][z]["changed"] = false
		end
	end
	return cestomapa
end
function updatniCestu(novaCesta)
	local newpath = {}
	for i=1,#novaCesta do
		newpath[i] = {x =  novaCesta[i].x, z = novaCesta[i].z }	
	end
	return newpath

end
	
-- @description scan whole map for hills of certain height and return them in a list {minX,minZ,x,z,maxX,maxZ} 
--				where minXZ are the top left corner and maxXZ are the bottom right corner of the hill
-- @argument gridSize - default 50 - the granularity of the scan 
-- @argument gridBased - default true - whether the return table is indexed by grid size or by amount of elements 
return function(DangerGrid,pos1,pos2,gridStep)
	local returnPath = {}	
	local dangerGrid = DangerGrid
	local maxX = #dangerGrid
	local start = positionToGridPoint(pos1,gridStep)
	local finish = positionToGridPoint(pos2, gridStep)
	local startX = start[1]
	local startZ = start[2]
	local finishX = finish[1]
	local finishZ = finish[2]
	local changed = true
	bb.sx = start
	bb.dg = dangerGrid
	bb.nb = getNeighbours(startX,startZ,dangerGrid)
	bb.dangerMin = math.max(dangerGrid[startX][startZ],dangerGrid[finishX][finishZ])
	--check jestli jsme bez cesty co hledat
	if startX == finishX and startZ == finishZ then return returnPath end
	
	--nastavim dangerMin dle startu a cile
	--generujeme frontu okoli
	--pridame do mapy cest ty, ktere jsou 
	--	-kratsi
	--	-bezpecnejsi
	local mapaCest = {} -- mapaCest[x][z] = {delka,danger}
	mapaCest = udelejMapu(dangerGrid)
	bb.dgt = dangerGrid[81][140]
	bb.mct = mapaCest[81][140].danger
	bb.mc = mapaCest
	
	local fronta = {}
	mapaCest[startX][startZ].delka = 0
	mapaCest[startX][startZ].cesta[1] = {x = startX,z = startZ}
	mapaCest[startX][startZ].changed = true
	bb.logger = {}
	Spring.Echo("search initialised")
	local loops = 0
	while changed do
		Spring.Echo("loop start")
		changed = false 
		loops = loops +1
		--projdi mapu
		
		--najdes-li changed -> udelej okoli jako normalne
		local nasbod --prvni changed bod
		--jeho okoli -> rozvinu do nej cestu
		
		for u=1,#mapaCest do
			for v=1,#mapaCest[u] do
				if mapaCest[u][v].changed then 
					changed = true
					nasbod = {x = u, z = v}
					break
				end
			end	
			if changed then break end
		end
		if not changed then break end 
		fronta = getNeighbours(nasbod.x,nasbod.z,dangerGrid)	
		tododelka = mapaCest[nasbod.x][nasbod.z]["delka"] --delka cesty v uzlech 
		tododanger = mapaCest[nasbod.x][nasbod.z]["danger"] --max danger na ceste do bodu
		mapaCest[nasbod.x][nasbod.z].changed = false
		for k,i in ipairs(fronta) do
			-- i je dalsi bod cesty
			local potdanger = math.max(tododanger,dangerGrid[i.x][i.z])
			if mapaCest[i.x][i.z].delka < 0 then
				changed = true
				--bod nebyl navstiven, pridavame
				mapaCest[i.x][i.z].delka = tododelka+1 
				mapaCest[i.x][i.z].danger = potdanger
				mapaCest[i.x][i.z].changed = true
				mapaCest[i.x][i.z].cesta =  updatniCestu(mapaCest[nasbod.x][nasbod.z].cesta) --todocesta
				mapaCest[i.x][i.z].cesta[#mapaCest[i.x][i.z].cesta + 1] = {x = i.x, z = i.z}
				mapaCest[i.x][i.z].dangerSum = dangerGrid[i.x][i.z] + mapaCest[nasbod.x][nasbod.z].dangerSum
				--musime zkontrolovat jeho okoli 
				--add to fronta2
			else
				if mapaCest[i.x][i.z].danger >= potdanger and  mapaCest[i.x][i.z].dangerSum > dangerGrid[i.x][i.z] + mapaCest[nasbod.x][nasbod.z].dangerSum then
					--byli jsme navstiveni, ale nova je bezpecnejsi
					changed = true
					Spring.Echo("processing " .. dump(i))
					mapaCest[i.x][i.z].delka = tododelka+1 
					mapaCest[i.x][i.z].danger = potdanger
					mapaCest[i.x][i.z].cesta = {}
					mapaCest[i.x][i.z].changed = true
					--local tmpath = mapaCest[nasbod.x][nasbod.z].cesta
					mapaCest[i.x][i.z].cesta = updatniCestu(mapaCest[nasbod.x][nasbod.z].cesta) --todocesta
					mapaCest[i.x][i.z].cesta[#mapaCest[i.x][i.z].cesta + 1] = {x = i.x, z = i.z} 
					mapaCest[i.x][i.z].dangerSum = dangerGrid[i.x][i.z] + mapaCest[nasbod.x][nasbod.z].dangerSum
					
				end
			end
		-- if mapaCest[i.x][i.z].delka < 0 -> mapaCest[i.x][i.z].delka = tododelka+1 && mapaCest[i.x][i.z].danger = max(mapaCest[i.x][i.z].danger, tododanger)
		-- else
		--  uz bylo navstiveno
		--  if mapaCest[i.x][i.z].danger < tododanger then 
		--		
		end
		
		
	
		if loops < -1000 then 
			Spring.Echo("Lloop end")
			for u=1,#mapaCest do
				for v=1,#mapaCest[u] do
					if mapaCest[u][v].delka > 0 then 
						Sensors.nota_mastr_ukol1.DrawSafePath(mapaCest[u][v].cesta)
					end
				end	
			end
			break
		else
		
		end
	
	end
	--returnPath[1] = start
	--returnPath[2]= finish
	returnPath = mapaCest[finishX][finishZ].cesta
	for u=1,#mapaCest do
				for v=1,#mapaCest[u] do
					if mapaCest[u][v].delka > 0 then 
						Sensors.nota_mastr_ukol1.DrawSafePath(mapaCest[u][v].cesta)
					end
				end	
			end
	
	return returnPath
end


