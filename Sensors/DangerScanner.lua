local sensorInfo = {
	name = "DangerScanner",
	desc = "Scans map for height",
	author = "PepeAmpere",
	date = "2017-05-20",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- instant, no caching

local GRID_STEP = 50 --how big a grid step is, for speedup, really 

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end
-- @description scan whole map for hills of certain height and return them in a list {minX,minZ,x,z,maxX,maxZ} 
--				where minXZ are the top left corner and maxXZ are the bottom right corner of the hill
-- @argument gridSize - default 50 - the granularity of the scan 
-- @argument gridBased - default true - whether the return table is indexed by grid size or by amount of elements 
return function(x1, x2, GridSize)

	local myID = Spring.GetMyTeamID()
	local gridSize = GridSize or 50
	local maxX = Game.mapSizeX
	local maxZ = Game.mapSizeZ
	local A = x1 or Vec3(0,0,0)
	local B = x2 or Vec3(maxX,0,maxZ)
	local teams = Spring.GetTeamList()
	local enemyTeams = {}
	local allEnemies = {}
	
	for i=1,#teams do
		if not Spring.AreTeamsAllied(myID,teams[i]) then
			enemyTeams[#enemyTeams+1] = teams[i]
		end	
	end
	bb.et = enemyTeams
	for	i=1,#enemyTeams do
		local enemyUnits = Spring.GetTeamUnits(enemyTeams[i])
		for j=1,#enemyUnits do
			allEnemies[enemyUnits[j]] = Vec3(Spring.GetUnitPosition(enemyUnits[j]))
		end	
	end
	
	return allEnemies
end