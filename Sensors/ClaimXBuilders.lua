local sensorInfo = {
	name = "GetAllBuilders",
	desc = "Vrati jednotky spravneho typu ze seznamu",
	author = "PepeAmpere",
	date = "2018-04-16",
	license = "MIT",
}


local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
--local ID = 0
function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end
return function(unitList,X)
	
	local matchingUnits = {}
	local claimed = 0
	if not unitList  then
		Logger.warn("No units available")
		return matchingUnits
	end
	
	for unitID,vlastnosti in pairs(unitList) do
		if (vlastnosti.designation == "new" or vlastnosti.designation == "free") and claimed < X then 
			vlastnosti.designation = "builder"
			claimed = claimed + 1
			matchingUnits[unitID] = vlastnosti
		end
		if claimed  == X then break end
	end
	return { claimedBuilders = matchingUnits,
			 count = claimed }
end