local sensorInfo = {
	name = "FindNewUnits",
	desc = "Sends data to example debug widget",
	author = "PepeAmpere",
	date = "2018-04-16",
	license = "MIT",
}


local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
--local ID = 0
function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end
return function(oldUnits)
	
	local currentUnits = Spring.GetTeamUnits(Spring.GetMyTeamID() )
	local newUnitsRev = {}
	for index,unitID in pairs(currentUnits) do
		if not oldUnits[unitID] then 
			newUnitsRev[unitID] = "free"
		end
	end
	
	
	
	return newUnitsRev
end