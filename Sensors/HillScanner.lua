local sensorInfo = {
	name = "HillScanner",
	desc = "Scans map for hills of certain height",
	author = "PepeAmpere",
	date = "2017-05-20",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- instant, no caching

local GRID_STEP = 50 --how big a grid step is, for speedup, really 

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description scan whole map for hills of certain height and return them in a list {minX,minZ,x,z,maxX,maxZ} 
--				where minXZ are the top left corner and maxXZ are the bottom right corner of the hill
-- @argument HillHeight - height of the hills to find 
return function(HillHeight)
	local hillHeight = HillHeight
	local maxX = Game.mapSizeX
	local maxZ = Game.mapSizeZ
	local returnHills = {}
	local hillRead = false
	local totalHills = 0
	local skip = false
	
	for x = 0,maxX,GRID_STEP do
		for z = 0,maxZ,GRID_STEP do
			--iterujeme po sloupcích 
			if((Spring.GetGroundOrigHeight(x,z) == hillHeight)) then
				--našli jsme hill				
				if(not hillRead) then
					--nový hill
					hillRead = true
					for j = 1,totalHills do
						--check jestli necteme jiz existujici hill
						local tmpX = returnHills[j]["x"]
						local tmpZ = returnHills[j]["z"]
						if x-GRID_STEP == tmpX then
							-- jedna se o jiz nalezeny hill
							returnHills[j]["x"] = x
							skip = true 
							break
						end
						--Spring.Echo(dump("x is " .. x ))
						--Spring.Echo(dump("z is " .. z ))
					end
					if(not skip) then	
						
						totalHills = totalHills + 1
						returnHills[totalHills] = {minX = x,minZ = z, x = x, z = z, maxX = x, maxz = z}		
					end
				else
					--stále stejný hill			
				
				end
				
				--Spring.Echo(dump(returnHills))
				--zaznamenat
			else
				if(hillRead) then
					returnHills[totalHills]["maxX"] = returnHills[totalHills]["x"]
					returnHills[totalHills]["maxZ"] = z - GRID_STEP
				end
				hillRead = false
				skip = false
			end
		end	
	end
	for i = 1,#returnHills do
		returnHills[i]["x"] = returnHills[i]["minX"] + (returnHills[i]["maxX"]-returnHills[i]["minX"])/2
		returnHills[i]["z"] = returnHills[i]["minZ"] + (returnHills[i]["maxZ"]-returnHills[i]["minZ"])/2
	end
	return returnHills
end