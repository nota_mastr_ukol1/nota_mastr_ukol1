local sensorInfo = {
	name = "GetOrder",
	desc = "Extrahuje objednavku ze seznamu",
	author = "PepeAmpere",
	date = "2018-04-16",
	license = "MIT",
}


local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
--local ID = 0
function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end
return function(orders)
	
	local prvniOrder = nil
	if not orders then
		Logger.Warn("No order available")
		return prvniOrder
	end
	local posledniOrder = 0
	for i,objednavka in ipairs(orders) do
		posledniOrder = i
		if i == 1 then 
			prvniOrder = objednavka
		else
		orders[i-1] = objednavka
		end	
	end
	orders[posledniOrder] = nil
	return { order = prvniOrder,
			 seznam = orders}
end