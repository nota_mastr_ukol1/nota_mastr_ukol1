local sensorInfo = {
	name = "DangerUpdater",
	desc = "Scans map for dangers",
	author = "PepeAmpere",
	date = "2017-05-20",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- instant, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end
-- @description scan whole map for enemy units and update this info in global.enemies
return function()

	local myID = Spring.GetMyTeamID()
	local maxX = Game.mapSizeX
	local maxZ = Game.mapSizeZ
	local A = x1 or Vec3(0,0,0)
	local B = x2 or Vec3(maxX,0,maxZ)
	local teams = Spring.GetTeamList()
	local enemyTeams = {}
	local allEnemies = {}
	--local second = false 
	if not global.allEnemies then 
		global.allEnemies = {}
		--second = true
	end
	
	for i=1,#teams do
		if not Spring.AreTeamsAllied(myID,teams[i]) then
			enemyTeams[#enemyTeams+1] = teams[i]
		end	
	end
	bb.et = enemyTeams
	for	i=1,#enemyTeams do
		local enemyUnits = Spring.GetTeamUnits(enemyTeams[i])
		for j=1,#enemyUnits do
			global.allEnemies[enemyUnits[j]] = Vec3(Spring.GetUnitPosition(enemyUnits[j]))
		end	
	end
	--[[local count = 0
	for x in pairs(global.allEnemies) do count = count + 1 end
	if second then 		
		bb.firstCount = count
		second = false
	end
	
	bb.allCount = count]]--
	return global.allEnemies
end