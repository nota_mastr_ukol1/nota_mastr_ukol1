local sensorInfo = {
	name = "GetUnitsForRescue",
	desc = "Sends data to example debug widget",
	author = "PepeAmpere",
	date = "2018-04-16",
	license = "MIT",
}


local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
--local ID = 0
function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end
return function(area)
    local myTeam = Spring.GetMyTeamID() 
	local myUnits = Spring.GetTeamUnits(myTeam)
	local myCategory = Categories.Common.groundUnits
	local myFilteredUnits = Sensors.core.FilterUnitsByCategory(myUnits,myCategory)
	local unitsToExclude = {}
	local targetArea = area
	if(targetArea ~= nill ) then
		--zatim bereme sphere area
		unitsToExclude = Sensors.core.FilterUnitsByCategory(Spring.GetUnitsInSphere(targetArea[1], targetArea[2], targetArea[3], targetArea[4] , myTeam ),myCategory) 
	end
	local unitsToExcludeRev = {}
	for i,v in ipairs(unitsToExclude) do
		unitsToExcludeRev[v] = i 
	end
	local myUnitsToRescue = {}
	local f = 1
	for	u=1,#myFilteredUnits do
		if(unitsToExcludeRev[myFilteredUnits[u]] == nil) then
			myUnitsToRescue[f] = myFilteredUnits[u]
			f = f+1
		end
	end
	local myUnitsToRescueRev = {}
	for i,v in ipairs(myUnitsToRescue) do
		myUnitsToRescueRev[v] = "not_rescued" --i 
	end
	--[[
	bb["myUnits"] = myUnits
	bb["myFilteredUnits"] = myFilteredUnits
	bb["unitsToExclude"] = unitsToExclude
	bb["unitsToExcludeRev"] = unitsToExcludeRev
	bb["myUnitsToRescue"] = myUnitsToRescue
	bb["myUnitsToRescueRev"] = myUnitsToRescueRev
    
	return { 
				unitsToRescue = myUnitsToRescue,
				unitsToRescueRev = myUnitsToRescueRev
			}]]--
	return myUnitsToRescueRev
end