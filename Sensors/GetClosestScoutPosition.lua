
local sensorInfo = {
	name = "GetClosestScoutPosition",
	desc = "Poda",
	author = "PepeAmpere",
	date = "2018-04-16",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
--local ID = 0
function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

function vecDist(v1,v2)

	return math.sqrt(((v1.x - v2.x)*(v1.x - v2.x)+ (v1.z - v2.z)*(v1.z - v2.z)))

end

return function(ScoutID,TargetID,TargetIsAPosition)
	
	local sid = ScoutID
	local tid = TargetID
	
	TargetIsAPosition = TargetIsAPosition or false
	
	if not Spring.ValidUnitID(sid) then	
		Spring.Echo("Invalid ScoutID --> "..sid)
	end
	
	
	local sX, sY, sZ = Spring.GetUnitPosition(sid)	
	local scoutPos = Vec3(sX,sY,sZ)
	local pos = scoutPos
	local posX,posY,posZ
	
	
	if TargetIsAPosition then
		Spring.Echo("target is a position")
		posX = tid.x
		posY = tid.y
		posZ = tid.z
		
	else		
		Spring.Echo("target is an ID")
		if not Spring.ValidUnitID(TargetID) then	
			Spring.Echo("Invalid targetID --> "..tid)
			return pos
		end
		--if Spring.IsUnitInLos(tid,sid) then 
		--	return pos
		--end
		 posX,posY,posZ = Spring.GetUnitPosition(tid)	
		--Spring.Echo(posX..posY..posZ..sid)
	end
	
	local tposV = Vec3(posX,posY,posZ)
	--Spring.Echo(posX)
	--Spring.Echo(posY)
	--Spring.Echo(posZ)
	--Spring.Echo(posX..posY..posZ..sid)
	--local los = Spring.IsPosInLos(posX,posY,posZ,sid)
	--	Spring.Echo(los)
	local range = Spring.GetUnitSensorRadius(sid, "los" )
	local dist = scoutPos:Distance(tposV)
	Spring.Echo(dump(scoutPos))
	Spring.Echo(dump(tposV))
	Spring.Echo("distance " ..dist.." range "..range)
	dist = vecDist(scoutPos,tposV)
	Spring.Echo("distance " ..dist.." range "..range)
	if dist > range then
		--musime se posunout
		 	
		
		local overRange = dist/range
		local rangeRate = (overRange -1)*1.1
		local travelVector = tposV-scoutPos
		
		local travelDistance = rangeRate*range
		local travelRate = travelDistance/dist 
		
		local vecDiff = travelVector*travelRate
		Spring.Echo("overRange "..overRange.." travelRate "..travelRate.." vecDiff "..vecDiff.x)
		pos = scoutPos + vecDiff
	end
	
	return pos 
end

