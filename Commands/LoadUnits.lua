function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Unload units specified in the unit list from selected unit",
		parameterDefs = {
			{ 
				name = "UnitsToLoad",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "TransportToLoadTo", -- id of the selected unit
				variableType = "UnitID",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- constants
--local ORDER_TO_ISSUE = true
--local self.orderGiven = false

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
	--ORDER_TO_ISSUE = true
	self.orderGiven = false
end

function Run(self, units, parameter)
	local unitsToLoad = parameter.UnitsToLoad -- list of unitIDs to unload 
	local transportID = parameter.TransportToLoadTo -- unitID of the transport		
	
	if(unitsToLoad == nil) then return FAILURE end
	if(transportID == nil) then return FAILURE end
	--transporter je dead
	if not Spring.ValidUnitID(transportID) then	return FAILURE	end
	--transporter neni transport
	if not UnitDefs[Spring.GetUnitDefID(transportID)].isTransport then	return FAILURE	end
	
	
	--SpringGiveOrderToUnit(transportID,CMD.LOAD_UNITS,{unitsToLoad[1]},{"shift"})
	
	--if(ORDER_TO_ISSUE) then
	if(not self.orderGiven) then
		for i = 1,#unitsToLoad do
			--load only alive units
			if Spring.ValidUnitID(unitsToLoad[i]) then	
				--Spring.Echo("Loading " ..unitsToLoad[i])
				SpringGiveOrderToUnit(transportID,CMD.LOAD_UNITS,{unitsToLoad[i]},{"shift"})	
			end
		end
		--ORDER_TO_ISSUE = false
		self.orderGiven = true
	end
	
	if #Spring.GetUnitIsTransporting(transportID) == #unitsToLoad then
        return SUCCESS
    end  
	return RUNNING
end


function Reset(self)
	ClearState(self)
end
