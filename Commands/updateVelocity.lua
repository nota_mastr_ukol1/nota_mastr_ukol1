function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Update velocity of the current unit according to the wind",
		parameterDefs = {
			{ 
				name = "updateVectorInfo", --parametres of the wind as returned by map.Wind() sensor 
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "unitID", -- id of the selected unit
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "unitVectorInfo", -- movement vector of the selected unit
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- constants
local THRESHOLD_STEP = 25
local THRESHOLD_DEFAULT = 0

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
	self.lastPointmanPosition = Vec3(0,0,0)
end

function Run(self, units, parameter)
	local updateVector = parameter.updateVectorInfo -- vector 
	local unitID = parameter.unitID -- unitID of the affected unit
	--local unitVector = parameter.unitVector -- vector of the unit {xdir,ydir}
	--local unitX = unitVector.xdir
	--local unitY = unitVector.ydir
	local difference = 0
	--Spring.Echo(dump(parameter.formation))
	--Spring.Echo("parameter.updateVectorInfo3")
	--Spring.Echo(parameter.updateVectorInfo.dirX)
	
	--Spring.Echo("parameter.unitVectorInfo")
	--Spring.Echo(parameter.unitVectorInfo)
	--Spring.Echo(math.abs(-5))
	
	--add wind value to the unit value 
	--parameter.updateVectorInfo.dirX = 10
	--[[
	if(parameter.updateVectorInfo.dirX >= 0 ) then
		if(parameter.unitVectorInfo.dirX >= 0) then
			--oba +
			difference = (parameter.updateVectorInfo.dirX - parameter.unitVectorInfo.dirX)
			Spring.Echo(difference)
		else
			-- + a -
			Spring.Echo("+ a -")
			difference = math.abs(parameter.updateVectorInfo.dirX - parameter.unitVectorInfo.dirX)
		end
	else
		if(parameter.unitVectorInfo.dirX < 0) then
			--oba -
			Spring.Echo("oba -")
		else
			-- - a +
			Spring.Echo("- a +")
		end
	end]]--
	parameter.unitVectorInfo.dirX = parameter.updateVectorInfo.dirX
	parameter.unitVectorInfo.dirZ = parameter.updateVectorInfo.dirZ
	
	--Spring.Echo("parameter.unitVectorInfo.xdir")
	--Spring.Echo(parameter.unitVectorInfo.dirX)
	--Spring.Echo("parameter.unitVectorInfo.zdir")
	--Spring.Echo(parameter.unitVectorInfo.dirZ)
	--parameter.torunitVec.xdir+updateVector.dirX
	return SUCCESS
	--[[
	-- validation
	if (#units > #formation) then
		Logger.warn("formation.move", "Your formation size [" .. #formation .. "] is smaller than needed for given count of units [" .. #units .. "] in this group.") 
		return FAILURE
	end
	
	-- pick the spring command implementing the move
	local cmdID = CMD.MOVE
	if (fight) then cmdID = CMD.FIGHT end

	local pointman = units[1] -- while this is running, we know that #units > 0, so pointman is valid
	local pointX, pointY, pointZ = SpringGetUnitPosition(pointman)
	local pointmanPosition = Vec3(pointX, pointY, pointZ)
	
	-- threshold of pointan success
	if (pointmanPosition == self.lastPointmanPosition) then 
		self.threshold = self.threshold + THRESHOLD_STEP 
	else
		self.threshold = THRESHOLD_DEFAULT
	end
	self.lastPointmanPosition = pointmanPosition
	
	-- check pointman success
	-- THIS LOGIC IS TEMPORARY, NOT CONSIDERING OTHER UNITS POSITION
	if (pointmanPosition:Distance(position) < self.threshold) then
		return SUCCESS
	else
		SpringGiveOrderToUnit(pointman, cmdID, position:AsSpringVector(), {})
		
		for i=2, #units do
			local thisUnitWantedPosition = pointmanPosition + formation[i]
			SpringGiveOrderToUnit(units[i], cmdID, thisUnitWantedPosition:AsSpringVector(), {})
		end
		
		return RUNNING
	end]]--
end


function Reset(self)
	ClearState(self)
end
