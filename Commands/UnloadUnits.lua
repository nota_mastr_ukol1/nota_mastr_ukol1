function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Unload units specified in the unit list from selected unit",
		parameterDefs = {
			{ 
				name = "UnitsToUnload",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "TransportToUnloadFrom", -- id of the selected unit
				variableType = "UnitID",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "UnloadArea",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- constants
--local ORDER_TO_ISSUE = true

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
	--ORDER_TO_ISSUE = true
	self.orderGiven = false
end

function Run(self, units, parameter)
	local unitsToUnload = parameter.UnitsToUnload -- list of unitIDs to unload 
	local transportID = parameter.TransportToUnloadFrom -- unitID of the transport		
	local unloadArea = parameter.UnloadArea -- area where to unload { x, y, z, radius } 
	if(unitsToUnload == nil) then return FAILURE end
	if(transportID == nil) then return FAILURE end
	--TODO failures and stuff
	
	
	--transporter je dead
	if not Spring.ValidUnitID(transportID) then	return FAILURE	end
	
	
	--transporter neni transport
	if not UnitDefs[Spring.GetUnitDefID(transportID)].isTransport then return FAILURE	end
	
	--Spring.Echo("running unload")
	--Logger.warn(unitsToUnload[1])
	--if(ORDER_TO_ISSUE) then
	if(not self.orderGiven) then 
		Spring.GiveOrderToUnit(transportID,CMD.UNLOAD_UNITS,unloadArea ,{"shift"})
		--Spring.Echo("Unloading units")
		for i = 1,#unitsToUnload do
			--Spring.Echo("unloading " .. unitsToUnload[i])
			--Logger.warn("unloading " .. unitsToUnload[i])
			--Spring.GiveOrderToUnit(transportID,CMD.UNLOAD_UNITS,{unitsToUnload[i]},{"shift"})
		end
		--ORDER_TO_ISSUE = false
		self.orderGiven = true
	end
	
	--transport je prazdny -> success 
	if #Spring.GetUnitIsTransporting(transportID) == 0 then
		Spring.GiveOrderToUnit(unitsToUnload[1],CMD.STOP,{},{"shift"})
        return SUCCESS
    end  
	return RUNNING
end


function Reset(self)
	ClearState(self)
end
