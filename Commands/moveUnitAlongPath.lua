function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move one unit along specified path - queue move orders ",
		parameterDefs = {
			{ 
				name = "unitID",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			-- @parameter unitID - unitID of the unit to move
			{ 
				name = "path",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			-- @parameter path [array] - list of Vec3
			--[[ local example = {
				[1] = Vec3(0,0,0),
				[2] = Vec3(10,0,0),
				[3] = Vec3(-10,0,0),
			}
			]]--			
			{ 
				name = "fight",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "false",
			}
		}
	}
end

-- constants
local THRESHOLD_STEP = 25
local THRESHOLD_DEFAULT = 10

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
	self.initialised = false
end

function Run(self, units, parameter)
	local id = parameter.unitID[1]
	local cesta = parameter.path
	local fight = parameter.fight -- boolean
	
	
	if(id == nil) then 
		--Spring.Echo("nil")
		return FAILURE 
	end
	if not Spring.ValidUnitID(id) then	
		--Spring.Echo("ded")
		bb.uidx = id
		return FAILURE	
	end
	
	if not cesta then return SUCCESS end
	if #cesta == 0 then return SUCCESS end
	
	local cmdID = CMD.MOVE
	if (fight) then cmdID = CMD.FIGHT end
	bb.cst = cesta[1]
	--SpringGiveOrderToUnit(id, cmdID, cesta[1]:AsSpringVector(), {"shift"})
	
	local pointX, pointY, pointZ = SpringGetUnitPosition(id)
	local pos = Vec3(pointX, 0, pointZ)
	if not self.initialised then 
		for i=1,#cesta do
			SpringGiveOrderToUnit(id, cmdID, cesta[i]:AsSpringVector(), {"shift"})
		end
		--Spring.Echo("oops!")
		self.initialised = true
	end
	if pos:Distance(Vec3(cesta[#cesta].x,0,cesta[#cesta].z)) < 100 then 
		return SUCCESS
	end
	
		if not bb.distances then bb.distances = {} end
		bb.distances[id] = pos:Distance(cesta[#cesta])
		return RUNNING
	
end


function Reset(self)
	ClearState(self)
end
