function getInfo()
    return {
        onNoUnits = SUCCESS, -- instant success
        tooltip = "Returns all allied units that are not in target area and fall under unit category.",
        parameterDefs = {
			{ 
				name = "unitCategory", --kategorie jednotek ktere ma najit na mape 
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "targetArea", --cilova oblast kam chceme jednotky evakuovat
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
		
		
		}
		
    }
end

function Run(self, units, parameter)
	bb.testrescue = 666
    local myTeam = Spring.GetMyTeamID() 
	local myUnits = Spring.GetTeamUnits(myTeam)
	local myCategory = Categories.Common.groundUnits
	bb.mycat = myCategory
	local myFilteredUnits = Sensors.core.FilterUnitsByCategory(myUnits,myCategory)
	bb.mfunits = myFilteredUnits
	local unitsToExclude = {}
	local targetArea = parameter.targetArea
	--bb["tA"] = parameter.targetArea
	if(targetArea ~= nill ) then
		--zatim bereme sphere area
		unitsToExclude = Sensors.core.FilterUnitsByCategory(Spring.GetUnitsInSphere(targetArea[1], targetArea[2], targetArea[3], targetArea[4] , myTeam ),myCategory) 
	end
	local unitsToExcludeRev = {}
	for i,v in ipairs(unitsToExclude) do
		unitsToExcludeRev[v] = i 
	end
	local myUnitsToRescue = {}
	local f = 1
	for	u=1,#myFilteredUnits do
		if(unitsToExcludeRev[myFilteredUnits[u]] == nil) then
			myUnitsToRescue[f] = myFilteredUnits[u]
			f = f+1
		end
	end
	local myUnitsToRescueRev = {}
	for i,v in ipairs(myUnitsToRescue) do
		myUnitsToRescueRev[v] = i 
	end
	bb["myUnits"] = myUnits
	bb["myFilteredUnits"] = myFilteredUnits
	bb["unitsToExclude"] = unitsToExclude
	bb["unitsToExcludeRev"] = unitsToExcludeRev
	bb["myUnitsToRescue"] = myUnitsToRescue
	bb["myUnitsToRescueRev"] = myUnitsToRescueRev
    return SUCCESS
end


function Reset(self)
    return self
end