function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move one unit along specified path - queue move orders ",
		parameterDefs = {
			{ 
				name = "unitID",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- constants
local THRESHOLD_STEP = 25
local THRESHOLD_DEFAULT = 10

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
	self.initialised = false
end

function Run(self, units, parameter)
	local ids = parameter.unitID
	if not ids then return FAILURE end 
	if #ids == 0 then return FAILURE end 
	
	bb.test = ids[1] 
	
	
	local maxX = Game.mapSizeX
	local maxZ = Game.mapSizeZ
	local scoutCount = #ids
	local step = maxX/(scoutCount+1)
	local start = {}
	local cil = {}
	local cmdID = CMD.MOVE
	local succeeded = true 
	bb.cunt = scoutCount
		--local uid = ids[i]
		
		
	if not self.initialised then 
		--prvotni udani rozkazu
		for i,uid in pairs(ids) do 
			--naplanujeme cestu pro dotycneho
			--local uid = ids[i]
			
			start[i] = Vec3(step*i,Spring.GetGroundOrigHeight(step*i,1),1)
			SpringGiveOrderToUnit(uid, cmdID, start[i]:AsSpringVector(), {"shift"})			
			cil[i] = Vec3(step*i,Spring.GetGroundOrigHeight(step*i,maxZ),maxZ)
			SpringGiveOrderToUnit(uid, cmdID, cil[i]:AsSpringVector(), {"shift"})
		end
		self.initialised = true 
		
		bb.start = start
		bb.cil = cil[1].x
	end 
	
	--bb.dist = {}
	--jen kontrolujeme zda doleteli do cile / umreli 
	for i = 1,#ids do 
		local pointX, pointY, pointZ = SpringGetUnitPosition(ids[i])
		local pos = Vec3(pointX, 0, pointZ)
		
		--bb.dist[i] = pos:Distance(Vec3(cil[i].x,0,cil[i].z))
		bb.xx =  pos:Distance(cil[i]) --pos:Distance(Vec3(cil[i].x,0,cil[i].z)) --pos:Distance(Vec3(cil[i].x,0,cil[i].z)) > 100 
		if ( pos:Distance(Vec3(cil[i].x,0,cil[i].z)) > 100 ) and Spring.ValidUnitID(ids[i]) then 
			--jsme moc daleko a pritom nazivu
			succeeded = false
		end	
			
	end
	
	
	if succeeded then 
		return SUCCESS 
	else
		return RUNNING
	end
	 --return RUNNING--[[]]--
end


function Reset(self)
	ClearState(self)
end
