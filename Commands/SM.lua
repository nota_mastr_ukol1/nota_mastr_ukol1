function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move one unit along specified path - queue move orders ",
		parameterDefs = {
			{ 
				name = "unitID",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- constants
local THRESHOLD_STEP = 25
local THRESHOLD_DEFAULT = 10

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
	self.initialised = false
end

function Run(self, units, parameter)
	local ids = parameter.unitID
	if not ids then return SUCCESS end 
	if #ids == 0 then return SUCCESS end 
	local maxX = Game.mapSizeX
	local maxZ = Game.mapSizeZ
	local scoutCount = #ids
	local step = maxX/(scoutCount+1)
	local start = {}
	local cil = {}
	local cmdID = CMD.MOVE
	local succeeded = true 
	if not self.initialised then 
		--prvotni udani rozkazu
		for i,uid in pairs(ids) do 
			--naplanujeme cestu pro dotycneho
			start[i] = Vec3(step*i,Spring.GetGroundOrigHeight(step*i,1),1)
			SpringGiveOrderToUnit(uid, cmdID, start[i]:AsSpringVector(), {"shift"})			
			cil[i] = Vec3(step*i,Spring.GetGroundOrigHeight(step*i,maxZ),maxZ)
			SpringGiveOrderToUnit(uid, cmdID, cil[i]:AsSpringVector(), {"shift"})
		end
		self.initialised = true 
		
		bb.start = start
		bb.cil = cil
	end 
	--succeeded = false
	for i,uid in pairs(ids) do 
		local pointX, pointY, pointZ = SpringGetUnitPosition(ids[i])
		local pos = Vec3(pointX, 1, pointZ)
		if Spring.ValidUnitID(ids[i]) then
			if pos:Distance(Vec3(pointX,pointY,maxZ)) > 1000 then
				succeeded = false
				
			end
		end
	end
	if succeeded then 
		return SUCCESS 
	else
		return RUNNING
	end
end


function Reset(self)
	ClearState(self)
end
