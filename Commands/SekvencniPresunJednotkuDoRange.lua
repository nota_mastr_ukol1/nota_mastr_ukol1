function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move one unit along specified path - queue move orders ",
		parameterDefs = {
			{ 
				name = "unitID",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			-- @parameter unitID - unitID of the unit to move
			{ 
				name = "deepstrikingUnitID",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "path",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},--[[
			{ 
				name = "stav",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},]]--
			-- @parameter path [array] - list of Vec3
			--[[ local example = {
				[1] = Vec3(0,0,0),
				[2] = Vec3(10,0,0),
				[3] = Vec3(-10,0,0),
			}
			]]--			
			{ 
				name = "fight",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "false",
			}
		}
	}
end

-- constants
local THRESHOLD_STEP = 25
local THRESHOLD_DEFAULT = 10

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
	self.initialised = false
end

function Run(self, units, parameter)
	local id = parameter.unitID[1]
	local cesta = parameter.path
	local fight = parameter.fight -- boolean
	--local stav = parameter.stav --  { waiting, loading, loaded,  transporting, ?? 
	local deepstrikingUnitID = parameter.deepstrikingUnitID[1]

	local unloadX = cesta[#cesta].x
	local unloadZ = cesta[#cesta].z
	local unloadY = Spring.GetGroundHeight(unloadX,unloadZ)
	
	local unloadArea = {unloadX,unloadY,unloadZ,200}
	
	--[[
	if not global.AllUnits.stav then 
		Logger.warn("watch out not listed unit encountered")
		global.AllUnits.stav = {} end]]--
	if not global.AllUnits[id].stav then 
		Logger.warn("watch out not listed unit encountered")
		global.AllUnits[id].stav = "waiting" end 
	
	--Spring.Echo("stav je "..global.AllUnits[id].stav)
	if(id == nil) then 
		Spring.Echo("nil")
		return FAILURE 
	end
	if not Spring.ValidUnitID(id) then	
		Spring.Echo("ded")
		bb.uidx = id
		return FAILURE	
	end
	if not Spring.ValidUnitID(deepstrikingUnitID) then	
		Spring.Echo("striker ded")
		Spring.Echo(parameter.deepstrikingUnitID[1])
		return FAILURE						
	end
	
	if not cesta then return SUCCESS end
	if #cesta == 0 then return SUCCESS end
	
	local cmdID = CMD.MOVE
	if (fight) then cmdID = CMD.FIGHT end
	--bb.cst = cesta[1]
	--SpringGiveOrderToUnit(id, cmdID, cesta[1]:AsSpringVector(), {"shift"})
	
	local pointX, pointY, pointZ = SpringGetUnitPosition(id)
	local pos = Vec3(pointX, 0, pointZ)
	
	if global.AllUnits[id].stav == "waiting" then
		--naloz jednotku
		--Spring.Echo("testik")
		SpringGiveOrderToUnit(id,CMD.LOAD_UNITS,{deepstrikingUnitID},{"shift"})
		global.AllUnits[id].stav = "loading"
		--Spring.Echo("ok ok")
		
		return SUCCESS
	end
	if global.AllUnits[id].stav == "loading" then
		--mam nalozeno??
		if Spring.GetUnitIsTransporting(id)[1] ==  deepstrikingUnitID then 
			--dej move orders
			for i=1,#cesta do
				SpringGiveOrderToUnit(id, cmdID, cesta[i]:AsSpringVector(), {"shift"})
			end
			global.AllUnits[id].stav = "transporting"
			return SUCCESS
		else
			--Spring.Echo("I"..id.." have loaded " )--.. Spring.GetUnitIsTransporting(id)[1])
			--Spring.Echo(Spring.GetUnitIsTransporting(id)[1])
			--Spring.Echo("but this is what I should have "..deepstrikingUnitID)
			--Spring.Echo(deepstrikingUnitID)
			--Spring.Echo("---")
			return SUCCESS
			
		end
	end
	
	if global.AllUnits[id].stav == "transporting" then
		--jsme v cili?
		if pos:Distance(Vec3(cesta[#cesta].x,0,cesta[#cesta].z)) < 100 then 
			--dej unload orders
			Spring.GiveOrderToUnit(id,CMD.UNLOAD_UNITS,unloadArea ,{"shift"})
			global.AllUnits[id].stav = "unloading"
			return SUCCESS
		else
			return SUCCESS
		end
	end
	if global.AllUnits[id].stav == "unloading" then
		--mam vylozeno
		if #Spring.GetUnitIsTransporting(id) == 0 then
			Spring.GiveOrderToUnit(deepstrikingUnitID,CMD.STOP,{},{"shift"})
			global.AllUnits[id].stav = "done"
			return SUCCESS
		else
			return SUCCESS
		end
	end 
	if global.AllUnits[id].stav == "done" then return SUCCESS end
	return FAILURE
	
end


function Reset(self)
	ClearState(self)
end
